﻿var APP = window.APP || {}

APP.ListadoOrdenes = function () {
    var init = function () {
        console.log('ListadoOrdenes');
        cargarLista();
    }

    var cargarLista = function () {
        $.ajax({
            type: "POST",
            url: "../Views/ListadoOrdenes.aspx/ObtenerOrden",
            dataType: "json",
            data: JSON.stringify({
                pComercioId: 1
            }),
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                var data = JSON.parse(response.d);
                var content = '';
                for (var i = 0; i < data.length; i++) {
                    content = "<li class='pending-booking'>" +
                        "<div class='list-box-listing bookings'>" +
                        "<div class='list-box-listing-img'>" +
                        "<img src='http://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&s=120' alt=''/>" +
                        "</div>" +
                        "<div class='list-box-listing-content'>" +
                        "<div class='inner'>" +
                        "<h3>" + data[i].nombre + " " + data[i].apellidos + "</h3>" +
                        "<div class='inner-booking-list'>" +
                        "<h5>Fecha de entrega:</h5>" +
                        "<ul class='booking-list'>" +
                        "<li class='highlighted'>" + data[i].fechaEntregaString + "</li>" +
                        "</ul>" +
                        "</div>" +
                        "<div class='inner-booking-list'>" +
                        "<h5>Teléfono:</h5>" +
                        "<ul class='booking-list'>" +
                        "<li class='highlighted'>" + data[i].telefono + "</li>" +
                        "</ul>" +
                        "</div>" +
                        "<div class='inner-booking-list'>" +
                        "<h5>Detalle de la orden:</h5>" +
                        "<ul class='booking-list'>" +
                        "<li class='highlighted'><a href='#'>Ver detalle</a></li>" +
                        "</ul>" +
                        "</div>" +
                        "</div>" +
                        "</div>" +
                        "</div>" +
                        "<div class='buttons-to-right'>" +
                        "<a href='#' class='button gray approve'><i class='sl sl-icon-check'></i>Cambiar Estado</a>" +
                        "</div>" +
                        "</li>";
                    $('#listaOrdenes').append(content);
                }
            },
            failure: function (response) {
                swal("Error al cargar la lista", "", "error");
            },
            error: function (response) {
                swal("Error al cargar la lista", "", "error");
            }
        });
    }

    return {
        init: init,
        cargarLista: cargarLista
    }
}();
$(document).ready(function () {
    APP.ListadoOrdenes.init();
});