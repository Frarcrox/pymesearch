﻿var APP = window.APP || {}

APP.Login = function () {
    console.log("Login");
}();

$(document).ready(function () {
    $("#btnLogin").click(function () {
        $("#loginModal").modal();
    });
});

$(document).ready(function () {
    $("#btnRecuperarContrasenna").click(function () {
        $("#RecuperarContrasennaModal").modal();
        $("#loginModal").modal("toggle");
    });
});

$(document).ready(function () {
    $("#btnClose").click(function () {
        $("#RecuperarContrasennaModal").modal("toggle");
        $("#loginModal").modal();
    });
});