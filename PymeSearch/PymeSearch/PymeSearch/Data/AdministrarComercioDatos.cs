﻿using MySql.Data.MySqlClient;
using PowerLine.CapaDatos;
using PymeSearch.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace PymeSearch.Data
{
    public class AdministrarComercioDatos
    {
        public static bool CrearComercio(Comercio comercio)
        {
            bool resp = false;

            try
            {
                Database db = DatabaseFactory.CreateDatabase("ConnectMySQL");
                MySqlCommand command = new MySqlCommand("SP_PS_User_Insert");
                command.CommandType = CommandType.StoredProcedure;
                //command.Parameters.AddWithValue("@TC_UserName", pUsuario.usuario);
                //command.Parameters.AddWithValue("@TC_Password", pUsuario.contrasenna);
                command.Parameters.AddWithValue("@TN_UserType", 1);
                DataSet ds = db.ExecuteReader(command, "tps_user");
                resp = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return resp;
        }
    }
}