﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PowerLine.CapaDatos
{
   public  class Database : IDisposable
    {
        /*IDisposable permite a los objetos liberar los recursos que tengan correctamente y de forma determinista. 
         * Si en el código anteriorprodujese una excepción se corre el riesgo de no liberar los recursos. 
         * En el mejor de los casos, esos recursos se liberarían
         * y en el peor de los casos nunca se liberarían.*/
        public MySqlConnection Conexion { get; set; }

        public MySqlDataReader ExecuteReader(MySqlCommand sqlCommand)
        {
            MySqlDataReader lector = null;
            try
            {
                sqlCommand.Connection = Conexion;
                lector = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);
                return lector;
            }
            catch (Exception ex)
            {
                ex.Source += " SQL: " + sqlCommand.CommandText.ToString();
                Log.Write(MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public DataSet Query(string sqlString)
        {
            DataSet dtResult = new DataSet();
            try
            {
                MySqlDataAdapter dataadapter = new MySqlDataAdapter(sqlString, Conexion);             
                dataadapter.Fill(dtResult);
                return dtResult;
            }
            catch (Exception ex)
            {
                //System.IO.File.AppendAllText(System.Configuration.ConfigurationManager.AppSettings["Bitacora"].ToString() + "BitacoraLocal.log", "No hay comunicacion con la Base de Datos");
                DataSet dterror = new DataSet();
                return dterror;
                ex.Source += " SQL: ";
                throw ex;
            }
            finally
            {

                  if (dtResult != null)
                    dtResult.Dispose();


            }
        }
        public bool Execute(string sqlString)
        {
            try
            {
                MySqlDataAdapter dataadapter = new MySqlDataAdapter(sqlString, Conexion);
                MySqlCommand command = new MySqlCommand(sqlString, Conexion);
                //Conexion.Open();

                int Cant = command.ExecuteNonQuery();

                Conexion.Close();
                if (Cant >= 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public DataTable QueryDT(string sqlString)
        {
            try
            {
                MySqlDataAdapter dataadapter = new MySqlDataAdapter(sqlString, Conexion);
                DataTable dtResult = new DataTable();
                dataadapter.Fill(dtResult);
                return dtResult;
            }
            catch (Exception ex)
            {
                //System.IO.File.AppendAllText(System.Configuration.ConfigurationManager.AppSettings["Bitacora"].ToString() + "BitacoraLocal.log", "No hay comunicacion con la Base de Datos");
                DataTable dterror = new DataTable();
                return dterror;
            }
        }
        public DataSet ExecuteReader(MySqlCommand sqlCommand, String tabla)
        {
            DataSet dsTabla = new DataSet();
            try
            {
                using (MySqlDataAdapter adaptador = new MySqlDataAdapter(sqlCommand))
                {
                    sqlCommand.Connection = Conexion;
                    dsTabla = new DataSet();
                    adaptador.Fill(dsTabla, tabla);
                }
                return dsTabla;
            }
            catch (Exception ex)
            {
                ex.Source += " SQL: " + sqlCommand.CommandText.ToString();
                Log.Write(MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
            finally
            {
                if (dsTabla != null)
                    dsTabla.Dispose();
            }
        }

        public int ExecuteNonQuery(MySqlCommand sqlCommand, IsolationLevel isolationLevel)
        {
            //IsolationLevel: Especifica el nivel de aislamiento de la transacción.
            using (MySqlTransaction transaccion = Conexion.BeginTransaction(isolationLevel))
            {
                int registrosafectados = 0;
                try
                {
                    sqlCommand.Connection = Conexion;
                    sqlCommand.Transaction = transaccion;
                    registrosafectados = sqlCommand.ExecuteNonQuery();

                    // Commit a la transacción
                    transaccion.Commit();

                    return registrosafectados;
                }

                catch (Exception ex)
                {
                    ex.Source += " SQL: " + sqlCommand.CommandText.ToString();
                    Log.Write(MethodBase.GetCurrentMethod().Name, ex);
                    throw ex;
                }
            }
        }

        public int ExecuteNonQuery(MySqlCommand sqlCommand)
        {

            int registrosafectados = 0;
            try
            {
                sqlCommand.Connection = Conexion;
                registrosafectados = sqlCommand.ExecuteNonQuery();
                return registrosafectados;
            }
            catch (Exception ex)
            {
                ex.Source += " SQL: " + sqlCommand.CommandText.ToString();
                Log.Write(MethodBase.GetCurrentMethod().Name, ex);
                throw ex;
            }
        }

        public void ExecuteNonQuery(ref MySqlCommand sqlCommand, IsolationLevel isolationLevel)
        {
            using (MySqlTransaction transaccion = Conexion.BeginTransaction(isolationLevel))
            {
                try
                {
                    sqlCommand.Connection = Conexion;
                    sqlCommand.Transaction = transaccion;
                    sqlCommand.ExecuteNonQuery();

                    // Commit a la transacción
                    transaccion.Commit();


                }

                catch (Exception ex)
                {
                    ex.Source += " SQL: " + sqlCommand.CommandText.ToString();
                    Log.Write(MethodBase.GetCurrentMethod().Name, ex);
                    throw ex;
                }
            }
        }

        public void ExecuteNonQuery(List<MySqlCommand> commands, IsolationLevel isolationLevel)
        {
            using (MySqlTransaction transaccion = Conexion.BeginTransaction(isolationLevel))
            {
                try
                {
                    foreach (MySqlCommand command in commands)
                    {
                        command.Connection = Conexion;
                        command.Transaction = transaccion;
                        command.ExecuteNonQuery();
                    }
                    // Commit a la transacción
                    transaccion.Commit();
                }

                catch (Exception ex)
                {
                    ex.Source += " SQL: " + commands.ToString();
                    Log.Write(MethodBase.GetCurrentMethod().Name, ex);

                    throw ex;
                }
                finally
                {
                }
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (Conexion != null)
                Conexion.Close();
        }
        #endregion
    }
}
