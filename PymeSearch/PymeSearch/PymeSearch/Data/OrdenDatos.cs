﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using PowerLine.CapaDatos;
using PymeSearch.Entities;

namespace PymeSearch.Data
{
    public class OrdenDatos
    {
        public static bool CreateOrden(Orden pOrden)
        {
            bool resp;

            try
            {
                Database db = DatabaseFactory.CreateDatabase("ConnectMySQL");
                MySqlCommand command = new MySqlCommand("SP_PS_Order_Insert");
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@TN_Shop", pOrden.comercio.Id);
                command.Parameters.AddWithValue("@TC_ClientName", pOrden.nombre);
                command.Parameters.AddWithValue("@TC_ClientLastName", pOrden.apellidos);
                command.Parameters.AddWithValue("@TC_CommentOrder", pOrden.comentario);
                command.Parameters.AddWithValue("@TF_PickupDate", pOrden.fechaEntrega);
                command.Parameters.AddWithValue("@TC_Phone", pOrden.telefono);
                command.Parameters.AddWithValue("@TN_Estado", 1);
                DataSet ds = db.ExecuteReader(command, "tps_order");
                resp = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return resp;
        }

        public static bool UpdateOrden(Orden pOrden)
        {
            bool resp;

            try
            {
                Database db = DatabaseFactory.CreateDatabase("ConnectMySQL");
                MySqlCommand command = new MySqlCommand("SP_PS_Order_Update");
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@TN_Order", pOrden.Id);
                command.Parameters.AddWithValue("@TN_Shop", pOrden.comercio.Id);
                command.Parameters.AddWithValue("@TC_ClientName", pOrden.nombre);
                command.Parameters.AddWithValue("@TC_ClientLastName", pOrden.apellidos);
                command.Parameters.AddWithValue("@TC_CommentOrder", pOrden.comentario);
                command.Parameters.AddWithValue("@TF_PickupDate", pOrden.fechaEntrega);
                command.Parameters.AddWithValue("@TC_Phone", pOrden.telefono);
                command.Parameters.AddWithValue("@TN_Estado", pOrden.idEstado);
                DataSet ds = db.ExecuteReader(command, "tps_order");
                resp = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return resp;
        }

        public static bool DeleteOrden(int pId)
        {
            bool resp;

            try
            {
                Database db = DatabaseFactory.CreateDatabase("ConnectMySQL");
                MySqlCommand command = new MySqlCommand("SP_PS_Order_Delete");
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@TN_Order", pId);
                DataSet ds = db.ExecuteReader(command, "tps_order");
                resp = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return resp;
        }

        public static List<Orden> getOrden(int pId)
        {
            List<Orden> ordenes = new List<Orden>();

            try
            {
                Database db = DatabaseFactory.CreateDatabase("ConnectMySQL");
                MySqlCommand command = new MySqlCommand("SP_PS_Order_Select");
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@TN_Shop", pId);
                DataSet ds = db.ExecuteReader(command, "tps_order");

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    Orden orden = new Orden();
                    orden.Id = Convert.ToInt32(row["TN_Order"].ToString());
                    orden.comercio = new Comercio() { Id = Convert.ToInt32(row["TN_Shop"].ToString()) };
                    orden.nombre = row["TC_ClientName"].ToString();
                    orden.apellidos = row["TC_ClientLastName"].ToString();
                    orden.comentario = row["TC_CommentOrder"].ToString();
                    orden.fechaEntrega = Convert.ToDateTime(row["TF_PickupDate"].ToString());
                    orden.telefono = row["TC_Phone"].ToString();
                    orden.idEstado = Convert.ToInt32(row["TN_Estado"].ToString());

                    if (orden.idEstado == 1)
                        orden.estado = "Pendiente";

                    if (orden.idEstado == 2)
                        orden.estado = "En Proceso";

                    if (orden.idEstado == 3)
                        orden.estado = "Finalizado";

                    ordenes.Add(orden);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return ordenes;
        }
    }
}