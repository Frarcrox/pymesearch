﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PymeSearch.Entities
{
    public class Item
    {
        public int Id { get; set; }
        public string nombre { get; set; }
        public double precio { get; set; }
        public TipoItem tipoItem { get; set; }
        public List<Imagen> imagen { get; set; }

        public int MyProperty { get; set; }
    }
}