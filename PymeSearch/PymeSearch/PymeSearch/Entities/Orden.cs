﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PymeSearch.Entities
{
    public class Orden
    {
        public int Id { get; set; }
        public string nombre { get; set; }
        public string apellidos { get; set; }
        public string telefono { get; set; }
        public List<Item> producto { get; set; }
        public DateTime fechaEntrega { get; set; }
        public string comentario { get; set; }
        public string estado { get; set; }
        public int idEstado { get; set; }
        public Comercio comercio { get; set; }
        public string fechaEntregaString
        {
            get
            {
                return fechaEntrega.ToString("dd-MM-yyyy");
            }
            set
            {
                fechaEntregaString = value;
            }
        }
    }
}