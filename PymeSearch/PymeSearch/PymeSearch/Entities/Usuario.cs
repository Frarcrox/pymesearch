﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PymeSearch.Entities
{
    public class Usuario
    {
        public int Id { get; set; }
        public string usuario { get; set; }
        public string contrasenna { get; set; }

        public int tipoUsuario { get; set; }
        public string fecha_Creado { get; set; }
    }
}