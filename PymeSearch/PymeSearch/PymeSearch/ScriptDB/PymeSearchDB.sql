CREATE DATABASE  IF NOT EXISTS `pymesearchdb` /*!40100 DEFAULT CHARACTER SET latin1 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `pymesearchdb`;
-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: pymesearchdb
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tps_categoryshop`
--

DROP TABLE IF EXISTS `tps_categoryshop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tps_categoryshop` (
  `TN_CategoryShop` int(11) NOT NULL AUTO_INCREMENT,
  `TC_Description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`TN_CategoryShop`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tps_categoryshop`
--

LOCK TABLES `tps_categoryshop` WRITE;
/*!40000 ALTER TABLE `tps_categoryshop` DISABLE KEYS */;
INSERT INTO `tps_categoryshop` VALUES (1,'Ferretería'),(2,'Pandadería'),(3,'Peluquería'),(4,'Mueblería'),(5,'Ebanisteria'),(6,'Motorepuestos'),(7,'Autorepuestos'),(8,'Taller de mecánica'),(9,'Soda');
/*!40000 ALTER TABLE `tps_categoryshop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tps_comment`
--

DROP TABLE IF EXISTS `tps_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tps_comment` (
  `TN_Comment` int(11) NOT NULL AUTO_INCREMENT,
  `TN_Shop` int(11) DEFAULT NULL,
  `TC_ClientName` varchar(45) DEFAULT NULL,
  `TC_ClientLastName` varchar(45) DEFAULT NULL,
  `TC_Description` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`TN_Comment`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tps_comment`
--

LOCK TABLES `tps_comment` WRITE;
/*!40000 ALTER TABLE `tps_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `tps_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tps_location`
--

DROP TABLE IF EXISTS `tps_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tps_location` (
  `TN_Location` int(11) NOT NULL AUTO_INCREMENT,
  `TN_Shop` int(11) DEFAULT NULL,
  `TC_Latitud` varchar(50) DEFAULT NULL,
  `TC_Longitude` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`TN_Location`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tps_location`
--

LOCK TABLES `tps_location` WRITE;
/*!40000 ALTER TABLE `tps_location` DISABLE KEYS */;
/*!40000 ALTER TABLE `tps_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tps_orden_detail`
--

DROP TABLE IF EXISTS `tps_orden_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tps_orden_detail` (
  `TN_Detail` int(11) NOT NULL AUTO_INCREMENT,
  `TN_Order` int(11) NOT NULL,
  `TN_Product` int(11) NOT NULL,
  PRIMARY KEY (`TN_Detail`,`TN_Order`),
  KEY `FK_Order_Order_Detail` (`TN_Order`),
  KEY `FK_ProdctOrderDetail` (`TN_Product`),
  CONSTRAINT `FK_Order_Order_Detail` FOREIGN KEY (`TN_Order`) REFERENCES `tps_order` (`TN_Order`),
  CONSTRAINT `FK_ProdctOrderDetail` FOREIGN KEY (`TN_Product`) REFERENCES `tps_product` (`TN_Product`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tps_orden_detail`
--

LOCK TABLES `tps_orden_detail` WRITE;
/*!40000 ALTER TABLE `tps_orden_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `tps_orden_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tps_order`
--

DROP TABLE IF EXISTS `tps_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tps_order` (
  `TN_Order` int(11) NOT NULL AUTO_INCREMENT,
  `TN_Shop` int(11) DEFAULT NULL,
  `TC_ClientName` varchar(20) DEFAULT NULL,
  `TC_ClientLastName` varchar(20) DEFAULT NULL,
  `TC_CommentOrder` varchar(100) DEFAULT NULL,
  `TF_PickupDate` datetime DEFAULT NULL,
  `TC_Phone` varchar(45) DEFAULT NULL,
  `TN_Estado` int(11) DEFAULT NULL,
  PRIMARY KEY (`TN_Order`),
  KEY `FK_Shop_idx` (`TN_Shop`),
  KEY `FK_Estado` (`TN_Estado`),
  CONSTRAINT `FK_Estado` FOREIGN KEY (`TN_Estado`) REFERENCES `tps_state` (`TN_State`),
  CONSTRAINT `FK_Shop` FOREIGN KEY (`TN_Order`) REFERENCES `tps_shop` (`TN_Shop`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tps_order`
--

LOCK TABLES `tps_order` WRITE;
/*!40000 ALTER TABLE `tps_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `tps_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tps_photoshop`
--

DROP TABLE IF EXISTS `tps_photoshop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tps_photoshop` (
  `TN_PhotoShop` int(11) NOT NULL AUTO_INCREMENT,
  `TC_Description` varchar(45) DEFAULT NULL,
  `TB_Photo` bit(20) DEFAULT NULL,
  `TN_Shop` int(11) NOT NULL,
  PRIMARY KEY (`TN_PhotoShop`),
  KEY `FL_photo` (`TN_Shop`),
  CONSTRAINT `FL_photo` FOREIGN KEY (`TN_Shop`) REFERENCES `tps_shop` (`TN_Shop`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tps_photoshop`
--

LOCK TABLES `tps_photoshop` WRITE;
/*!40000 ALTER TABLE `tps_photoshop` DISABLE KEYS */;
/*!40000 ALTER TABLE `tps_photoshop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tps_product`
--

DROP TABLE IF EXISTS `tps_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tps_product` (
  `TN_Product` int(11) NOT NULL AUTO_INCREMENT,
  `TC_Descripcion` varchar(100) DEFAULT NULL,
  `TN_Price` decimal(19,2) DEFAULT NULL,
  `TB_Photo` bit(20) DEFAULT NULL,
  PRIMARY KEY (`TN_Product`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tps_product`
--

LOCK TABLES `tps_product` WRITE;
/*!40000 ALTER TABLE `tps_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `tps_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tps_product_shop`
--

DROP TABLE IF EXISTS `tps_product_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tps_product_shop` (
  `TN_Product` int(11) NOT NULL,
  `TN_Shop` int(11) NOT NULL,
  PRIMARY KEY (`TN_Product`,`TN_Shop`),
  KEY `FK_Shop_Product_Shop` (`TN_Shop`),
  CONSTRAINT `FK_Product_Product_shop` FOREIGN KEY (`TN_Product`) REFERENCES `tps_product` (`TN_Product`),
  CONSTRAINT `FK_Shop_Product_Shop` FOREIGN KEY (`TN_Shop`) REFERENCES `tps_shop` (`TN_Shop`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tps_product_shop`
--

LOCK TABLES `tps_product_shop` WRITE;
/*!40000 ALTER TABLE `tps_product_shop` DISABLE KEYS */;
/*!40000 ALTER TABLE `tps_product_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tps_promotion`
--

DROP TABLE IF EXISTS `tps_promotion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tps_promotion` (
  `TN_Promotion` int(11) NOT NULL AUTO_INCREMENT,
  `TC_Descripcion` varchar(100) DEFAULT NULL,
  `TN_Product` int(11) NOT NULL,
  `TN_Shop` int(11) NOT NULL,
  `TF_Expiracion` datetime DEFAULT NULL,
  PRIMARY KEY (`TN_Promotion`),
  KEY `Fk_product` (`TN_Product`),
  KEY `FK_Promotion_Shop` (`TN_Shop`),
  CONSTRAINT `FK_Promotion_Shop` FOREIGN KEY (`TN_Shop`) REFERENCES `tps_shop` (`TN_Shop`),
  CONSTRAINT `Fk_product` FOREIGN KEY (`TN_Product`) REFERENCES `tps_product` (`TN_Product`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tps_promotion`
--

LOCK TABLES `tps_promotion` WRITE;
/*!40000 ALTER TABLE `tps_promotion` DISABLE KEYS */;
/*!40000 ALTER TABLE `tps_promotion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tps_schedule`
--

DROP TABLE IF EXISTS `tps_schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tps_schedule` (
  `TN_Shedule` int(11) NOT NULL AUTO_INCREMENT,
  `TF_Day` varchar(45) DEFAULT NULL,
  `TH_Starthour` time DEFAULT NULL,
  `TH_Endhour` varchar(45) DEFAULT NULL,
  `TN_Shop` int(11) DEFAULT NULL,
  PRIMARY KEY (`TN_Shedule`),
  KEY `FK_Schedule_Shop` (`TN_Shop`),
  CONSTRAINT `FK_Schedule_Shop` FOREIGN KEY (`TN_Shop`) REFERENCES `tps_shop` (`TN_Shop`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tps_schedule`
--

LOCK TABLES `tps_schedule` WRITE;
/*!40000 ALTER TABLE `tps_schedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `tps_schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tps_shop`
--

DROP TABLE IF EXISTS `tps_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tps_shop` (
  `TN_Shop` int(11) NOT NULL AUTO_INCREMENT,
  `TC_Name` varchar(45) DEFAULT NULL,
  `TC_Phone` varchar(45) DEFAULT NULL,
  `TC_Mail` varchar(45) DEFAULT NULL,
  `TC_Likn1` varchar(45) DEFAULT NULL,
  `TC_Likn2` varchar(45) DEFAULT NULL,
  `TN_User` int(11) DEFAULT NULL,
  `TN_Location` int(11) DEFAULT NULL,
  `TN_CategoryShop` int(11) DEFAULT NULL,
  `TN_Comment` int(11) DEFAULT NULL,
  PRIMARY KEY (`TN_Shop`),
  KEY `FK_User` (`TN_User`),
  KEY `FK_Location` (`TN_Location`),
  KEY `FK_Shopcat` (`TN_CategoryShop`),
  KEY `FK_Comment` (`TN_Comment`),
  CONSTRAINT `FK_Comment` FOREIGN KEY (`TN_Comment`) REFERENCES `tps_comment` (`TN_Comment`),
  CONSTRAINT `FK_Location` FOREIGN KEY (`TN_Location`) REFERENCES `tps_location` (`TN_Location`),
  CONSTRAINT `FK_Shopcat` FOREIGN KEY (`TN_CategoryShop`) REFERENCES `tps_categoryshop` (`TN_CategoryShop`),
  CONSTRAINT `FK_User` FOREIGN KEY (`TN_User`) REFERENCES `tps_user` (`TN_User`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tps_shop`
--

LOCK TABLES `tps_shop` WRITE;
/*!40000 ALTER TABLE `tps_shop` DISABLE KEYS */;
/*!40000 ALTER TABLE `tps_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tps_state`
--

DROP TABLE IF EXISTS `tps_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tps_state` (
  `TN_State` int(11) NOT NULL AUTO_INCREMENT,
  `TC_Description` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`TN_State`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tps_state`
--

LOCK TABLES `tps_state` WRITE;
/*!40000 ALTER TABLE `tps_state` DISABLE KEYS */;
INSERT INTO `tps_state` VALUES (1,'Pendiente'),(2,'En Proceso'),(3,'Finalizado');
/*!40000 ALTER TABLE `tps_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tps_user`
--

DROP TABLE IF EXISTS `tps_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tps_user` (
  `TN_User` int(11) NOT NULL AUTO_INCREMENT,
  `TC_UserName` varchar(100) DEFAULT NULL,
  `TC_Password` varchar(1000) DEFAULT NULL,
  `TF_CreatedDate` datetime DEFAULT NULL,
  `TN_UserType` int(11) NOT NULL,
  PRIMARY KEY (`TN_User`),
  KEY `FK_User_User_Type` (`TN_UserType`),
  CONSTRAINT `FK_User_User_Type` FOREIGN KEY (`TN_UserType`) REFERENCES `tps_user_type` (`TN_UserType`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tps_user`
--

LOCK TABLES `tps_user` WRITE;
/*!40000 ALTER TABLE `tps_user` DISABLE KEYS */;
INSERT INTO `tps_user` VALUES (1,'jcanoespinoza@gmail.com','123456','2020-11-27 00:00:00',1);
INSERT INTO `tps_user` VALUES (2,'edwarcenteno19@gmail.com','123456','2020-11-27 00:00:00',1);
INSERT INTO `tps_user` VALUES (3,'johancortes97@hotmail.com','123456','2020-11-27 00:00:00',1);
/*!40000 ALTER TABLE `tps_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tps_user_type`
--

DROP TABLE IF EXISTS `tps_user_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tps_user_type` (
  `TN_UserType` int(11) NOT NULL AUTO_INCREMENT,
  `TC_Description` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`TN_UserType`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tps_user_type`
--

LOCK TABLES `tps_user_type` WRITE;
/*!40000 ALTER TABLE `tps_user_type` DISABLE KEYS */;
INSERT INTO `tps_user_type` VALUES (1,'Administrador'),(2,'Comercio'),(3,'Cliente');
/*!40000 ALTER TABLE `tps_user_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'pymesearchdb'
--
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_CatgoryShop_Delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_CatgoryShop_Delete`(
TN_CategoryShop int(11))
BEGIN
DELETE FROM `pymesearchdb`.`tps_categoryshop`
WHERE `TN_CategoryShop` = TN_CategoryShop;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_CatgoryShop_Insert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_CatgoryShop_Insert`(
  TC_Description varchar(45))
BEGIN
INSERT INTO `pymesearchdb`.`tps_categoryshop`
	(`TC_Description`)
VALUES
	(TC_Description);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_CatgoryShop_Select` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_CatgoryShop_Select`()
BEGIN
SELECT `tps_categoryshop`.`TN_CategoryShop`,
       `tps_categoryshop`.`TC_Description`
FROM `pymesearchdb`.`tps_categoryshop`;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_CatgoryShop_Update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_CatgoryShop_Update`(
  TN_CategoryShop int(11),
  TC_Description varchar(45))
BEGIN
UPDATE `pymesearchdb`.`tps_categoryshop`
SET
	  `TN_CategoryShop` = TN_CategoryShop,
	  `TC_Description` =  TC_Description
WHERE `TN_CategoryShop` = TN_CategoryShop;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Comment_Delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Comment_Delete`(
  TN_Comment int(11))
BEGIN
DELETE FROM `pymesearchdb`.`tps_comment`
WHERE `TN_Comment` = TN_Comment;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Comment_Insert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Comment_Insert`(
TN_Shop int(11),
  TC_ClientName varchar(45),
  TC_ClientLastName varchar(45),
  TC_Description varchar(1000)
)
BEGIN
INSERT INTO `pymesearchdb`.`tps_comment`
	(`TN_Shop`,
	`TC_ClientName`,
	`TC_ClientLastName`,
	`TC_Description`)
VALUES
	(TN_Shop,
	 TC_ClientName,
	 TC_ClientLastName,
	 TC_Description);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Comment_Select` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Comment_Select`()
BEGIN
SELECT `tps_comment`.`TN_Comment`,
    `tps_comment`.`TN_Shop`,
    `tps_comment`.`TC_ClientName`,
    `tps_comment`.`TC_ClientLastName`,
    `tps_comment`.`TC_Description`
FROM `pymesearchdb`.`tps_comment`;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Comment_Update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Comment_Update`(
  TN_Comment int(11),
  TN_Shop int(11),
  TC_ClientName varchar(45),
  TC_ClientLastName varchar(45),
  TC_Description varchar(1000))
BEGIN
UPDATE `pymesearchdb`.`tps_comment`
SET
`TN_Comment` = TN_Comment,
`TN_Shop` = TN_Shop,
`TC_ClientName` = TC_ClientName,
`TC_ClientLastName` = TC_ClientLastName,
`TC_Description` = TC_Description
WHERE `TN_Comment` = TN_Comment;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Sp_PS_Location_Delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `Sp_PS_Location_Delete`(
 TN_Location int(11))
BEGIN
DELETE FROM `pymesearchdb`.`tps_location`
WHERE `TN_Location` = TN_Location;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Sp_PS_Location_Insert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `Sp_PS_Location_Insert`(
  TN_Shop int(11),
  TC_Latitud varchar(50),
  TC_Longitude varchar(50))
BEGIN
INSERT INTO `pymesearchdb`.`tps_location`
	(`TN_Shop`,
	`TC_Latitud`,
	`TC_Longitude`)
VALUES
	(TN_Shop,
	 TC_Latitud,
	 TC_Longitude);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Location_Select` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Location_Select`(
TN_Shop int(11))
BEGIN
SELECT `tps_shop`.`TN_Location`,
    `tps_shop`.`TN_Shop`,
    `tps_shop`.`TC_Latitud`,
    `tps_shop`.`TC_Longitude`
FROM `pymesearchdb`.`tps_location`
WHERE `TN_Shop` = TN_Shop;



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Sp_PS_Location_Update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `Sp_PS_Location_Update`(
  TN_Location int(11),
  TN_Shop int(11),
  TC_Latitud varchar(50),
  TC_Longitude varchar(50))
BEGIN
UPDATE `pymesearchdb`.`tps_location`
SET
`TN_Location` = TN_Location,
`TN_Shop` = TN_Shop,
`TC_Latitud` = TC_Latitud,
`TC_Longitude` = TC_Longitude
WHERE `TN_Location` = TN_Location;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Order_Delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Order_Delete`(
TN_Order int(11))
BEGIN
DELETE FROM `pymesearchdb`.`tps_order`
WHERE `TN_Order` = TN_Order;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Order_Detail_Delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Order_Detail_Delete`(
  TN_Order int(11))
BEGIN
DELETE FROM `pymesearchdb`.`tps_orden_detail`
WHERE `TN_Order` = TN_Order;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Order_Detail_Insert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Order_Detail_Insert`(
  TN_Order int(11),
  TN_Product int(11))
BEGIN
INSERT INTO `pymesearchdb`.`tps_orden_detail`
	(`TN_Order`,
	`TN_Product`)
VALUES
	(TN_Order,
	TN_Product);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Order_Detail_Select` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Order_Detail_Select`(
TN_Order int(11))
BEGIN
SELECT `tps_orden_detail`.`TN_Detail`,
       `tps_orden_detail`.`TN_Order`,
       `tps_orden_detail`.`TN_Product`
FROM `pymesearchdb`.`tps_orden_detail`
WHERE `TN_Order` = TN_Order;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Order_Detail_Update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Order_Detail_Update`(
  TN_Detail int(11),
  TN_Order int(11),
  TN_Product int(11))
BEGIN
	UPDATE `pymesearchdb`.`tps_orden_detail`
	SET
	`TN_Detail` = TN_Detail,
	`TN_Order` = TN_Order,
	`TN_Product` =TN_Product
	WHERE `TN_Detail` = TN_Detail;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Order_Insert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Order_Insert`(
  TN_Shop int(11),
  TC_ClientName varchar(20),
  TC_ClientLastName varchar(20),
  TC_CommentOrder varchar(100),
  TF_PickupDate datetime,
  TC_Phone varchar(45),
  TN_Estado int(11)
)
BEGIN
INSERT INTO `pymesearchdb`.`tps_order`
		(`TN_Shop`,
		`TC_ClientName`,
		`TC_ClientLastName`,
		`TC_CommentOrder`,
		`TF_PickupDate`,
		`TC_Phone`,
		`TN_Estado`)
VALUES
		(TN_Shop,
		TC_ClientName,
		TC_ClientLastName,
		TC_CommentOrder,
		TF_PickupDate,
		TC_Phone,
		TN_Estado);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Order_Select` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Order_Select`(
TN_Shop int(11))
BEGIN
SELECT `tps_order`.`TN_Order`,
    `tps_order`.`TN_Shop`,
    `tps_order`.`TC_ClientName`,
    `tps_order`.`TC_ClientLastName`,
    `tps_order`.`TC_CommentOrder`,
    `tps_order`.`TF_PickupDate`,
    `tps_order`.`TC_Phone`,
    `tps_order`.`TN_Estado`
FROM `pymesearchdb`.`tps_order`
WHERE `TN_Shop` = TN_Shop;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Order_Update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Order_Update`(
	 TN_Order int(11),
	 TN_Shop int(11),
	 TC_ClientName varchar(20),
	 TC_ClientLastName varchar(20),
	 TC_CommentOrder varchar(100),
	 TF_PickupDate datetime,
	 TC_Phone varchar(45),
	 TN_Estado int(11))
BEGIN
	UPDATE `pymesearchdb`.`tps_order`
	SET
	`TN_Order` = TN_Order,
	`TN_Shop` = TN_Shop,
	`TC_ClientName` = TC_ClientName,
	`TC_ClientLastName` = TC_ClientLastName,
	`TC_CommentOrder` = TC_CommentOrder,
	`TF_PickupDate` = TF_PickupDate,
	`TC_Phone` = TC_Phone,
	`TN_Estado` = TN_Estado
	WHERE `TN_Order` = TN_Order;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Photoshop_Delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Photoshop_Delete`(
TN_PhotoShop int(11))
BEGIN
DELETE FROM `pymesearchdb`.`tps_product_shop`
WHERE `TN_PhotoShop` = TN_PhotoShop;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Photoshop_Insert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Photoshop_Insert`(
  TC_Description varchar(45),
  TB_Photo bit(20),
  TN_Shop int(11)
)
BEGIN
INSERT INTO `pymesearchdb`.`tps_photoshop`
	(`TC_Description`,
	`TB_Photo`,
    `TN_Shop`)
VALUES
	(TC_Description,
	TB_Photo,
    TN_PhotoShop);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Photoshop_Select` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Photoshop_Select`(
TN_Shop int(11))
BEGIN
SELECT `tps_photoshop`.`TN_PhotoShop`,
       `tps_photoshop`.`TC_Description`,
       `tps_photoshop`.`TB_Photo`,
       `tps_photoshop`.`TN_Shop`
FROM `pymesearchdb`.`tps_photoshop`
WHERE `TN_Shop` = TN_Shop;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Photoshop_Update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Photoshop_Update`(
  TN_PhotoShop int(11),
  TC_Description varchar(45),
  TB_Photo bit(20),
  TN_Shop int(11))
BEGIN
UPDATE `pymesearchdb`.`tps_photoshop`
SET
	  `TN_PhotoShop` = TN_PhotoShop,
	  `TC_Description` = TC_Description,
	  `TB_Photo` = TB_Photo,
      `TN_Shop` = TN_Shop
WHERE `TN_PhotoShop` = TN_PhotoShop;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_ProductShop_Delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_ProductShop_Delete`()
BEGIN
DELETE FROM `pymesearchdb`.`tps_product_shop`
WHERE `TN_Product` = TN_Product;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_ProductShop_Insert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_ProductShop_Insert`(
  TN_Product int(11),
  TN_Shop int(11)
)
BEGIN
INSERT INTO `pymesearchdb`.`tps_product_shop`
	(`TN_Product`,
	`TN_Shop`)
VALUES
	(TN_Product,
	 TN_Shop);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_ProductShop_Select` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_ProductShop_Select`(
TN_Shop int(11))
BEGIN
SELECT `tps_product_shop`.`TN_Product`,
       `tps_product_shop`.`TN_Shop`
FROM `pymesearchdb`.`tps_product_shop`
WHERE `TN_Shop` = TN_Shop;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_ProductShop_Update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_ProductShop_Update`(
  TN_Product int(11),
  TN_Shop int(11))
BEGIN
	UPDATE `pymesearchdb`.`tps_product_shop`
	SET
	`TN_Product` = TN_Product,
	`TN_Shop` = TN_Shop
	WHERE `TN_Product` = TN_Product;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Product_Delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Product_Delete`(
TN_Product int(11))
BEGIN
DELETE FROM `pymesearchdb`.`tps_product`
WHERE `TN_Product` = TN_Product;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Product_Insert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Product_Insert`(
  TC_Descripcion varchar(100),
  TN_Price decimal(19,2),
  TB_Photo bit(20)
)
BEGIN
INSERT INTO `pymesearchdb`.`tps_product`
		(`TC_Descripcion`,
		`TN_Price`,
		`TB_Photo`)
VALUES
		(TC_Descripcion,
		TN_Price,
		TB_Photo);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Product_Select` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Product_Select`()
BEGIN
SELECT `tps_product`.`TN_Product`,
       `tps_product`.`TC_Descripcion`,
       `tps_product`.`TN_Price`,
       `tps_product`.`TB_Photo`
FROM `pymesearchdb`.`tps_product`;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Product_Update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Product_Update`(
  TN_Product int(11),
  TC_Descripcion varchar(100),
  TN_Price decimal(19,2),
  TB_Photo bit(20))
BEGIN
UPDATE `pymesearchdb`.`tps_product`
SET
	  `TN_Product` = TN_Product,
	  `TC_Descripcion` = TC_Descripcion,
	  `TN_Price` = TN_Price,
	  `TB_Photo` = TB_Photo
WHERE `TN_Product` = TN_Product;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Promotion_Delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Promotion_Delete`(
    TN_Promotion int(11))
BEGIN
	DELETE FROM `pymesearchdb`.`tps_promotion`
	WHERE `TN_Promotion` = TN_Promotion;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Promotion_Insert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Promotion_Insert`(
  TC_Descripcion varchar(100),
  TN_Product int(11),
  TN_Shop int(11),
  TF_Expiracion datetime
)
BEGIN
INSERT INTO `pymesearchdb`.`tps_promotion`
		(`TN_Promotion`,
		`TC_Descripcion`,
		`TN_Product`,
        `TN_Shop`,
		`TF_Expiracion`)
VALUES
		(TN_Promotion,
		 TC_Descripcion,
		 TN_Product,
         TN_Shop,
		 TF_Expiracion);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Promotion_Select` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Promotion_Select`(
TN_Shop int(11))
BEGIN
SELECT `tps_promotion`.`TN_Promotion`,
       `tps_promotion`.`TC_Descripcion`,
       `tps_promotion`.`TN_Product`,
       `tps_promotion`.`TN_Shop`,
       `tps_promotion`.`TF_Expiracion`
FROM `pymesearchdb`.`tps_promotion`
WHERE `TN_Shop` = TN_Shop;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Promotion_Update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Promotion_Update`(
    TN_Promotion int(11),
    TC_Descripcion varchar(100),
    TN_Product int(11),
    TN_Shop int(11),
    TF_Expiracion datetime)
BEGIN
	UPDATE `pymesearchdb`.`tps_promotion`
	SET
	`TN_Promotion` = TN_Promotion,
	`TC_Descripcion` = TC_Descripcion,
	`TN_Product` = TN_Product,
    `TN_Shop` = TN_Shop,
	`TF_Expiracion` = TF_Expiracion
	WHERE `TN_Promotion` = TN_Promotion;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Schedule_Delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Schedule_Delete`(
  TN_Shedule int(11))
BEGIN

DELETE FROM `pymesearchdb`.`tps_schedule`
WHERE `TN_Shedule` = TN_Shedule ;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Schedule_Insert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Schedule_Insert`(
  TF_Day varchar(45),
  TH_Starthour time,
  TH_Endhour varchar(45),
  TN_Shop int(11))
BEGIN
INSERT INTO `pymesearchdb`.`tps_schedule`
	(`TN_Shedule`,
	`TF_Day`,
	`TH_Starthour`,
	`TH_Endhour`,
	`TN_Shop`)
VALUES
	(TN_Shedule,
	 TF_Day,
	 TH_Starthour,
	 TH_Endhour,
	 TN_Shop);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Schedule_Select` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Schedule_Select`(
TN_Shop int(11))
BEGIN
SELECT `tps_schedule`.`TN_Shedule`,
       `tps_schedule`.`TF_Day`,
       `tps_schedule`.`TH_Starthour`,
	   `tps_schedule`.`TH_Endhour`,
       `tps_schedule`.`TN_Shop`
FROM   `pymesearchdb`.`tps_schedule`
WHERE `TN_Shop` = TN_Shop ;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Schedule_Update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Schedule_Update`(
  TN_Shedule int(11),
  TF_Day varchar(45),
  TH_Starthour time,
  TH_Endhour varchar(45),
  TN_Shop int(11))
BEGIN

UPDATE `pymesearchdb`.`tps_schedule`
SET
`TN_Shedule` = TN_Shedule,
`TF_Day` = TF_Day,
`TH_Starthour` = TH_Starthour,
`TH_Endhour` = TH_Endhour,
`TN_Shop` = TN_Shop
WHERE `TN_Shedule` = TN_Shedule;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Shop_Delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Shop_Delete`(
TN_Shop int(11))
BEGIN

DELETE FROM `pymesearchdb`.`tps_shop`
WHERE `TN_Shop` = TN_Shop;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Shop_Insert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Shop_Insert`(
TC_Name varchar(45),
TC_Phone varchar(45),
TC_Mail varchar(45),
TC_Password varchar(45),
TC_Likn1 varchar(45),
TC_Likn2 varchar(45),
TN_Location int(11),
TN_CategoryShop int(11),
TN_Comment int(11)
)
BEGIN


INSERT INTO `pymesearchdb`.`tps_shop`
		(`TC_Name`,
		`TC_Phone`,
		`TC_Mail`,
		`TC_Password`,
		`TC_Likn1`,
		`TC_Likn2`,
		`TN_Location`,
		`TN_CategoryShop`,
		`TN_Comment`)
VALUES
		(TC_Name,
		 TC_Phone,
		 TC_Mail,
		 TC_Password,
		 TC_Likn1,
		 TC_Likn2,
		 TN_Location,
		 TN_CategoryShop,
		 TN_Comment);


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Shop_Select` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Shop_Select`()
BEGIN
SELECT `tps_shop`.`TN_Shop`,
    `tps_shop`.`TC_Name`,
    `tps_shop`.`TC_Phone`,
    `tps_shop`.`TC_Mail`,
    `tps_shop`.`TC_Password`,
    `tps_shop`.`TC_Likn1`,
    `tps_shop`.`TC_Likn2`,
    `tps_shop`.`TN_Location`,
    `tps_shop`.`TN_CategoryShop`,
    `tps_shop`.`TN_Comment`
FROM `pymesearchdb`.`tps_shop`;



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_Shop_Update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_Shop_Update`(
TN_Shop int(11),
TC_Name varchar(45),
TC_Phone varchar(45),
TC_Mail varchar(45),
TC_Password varchar(45),
TC_Likn1 varchar(45),
TC_Likn2 varchar(45),
TN_Location int(11),
TN_CategoryShop int(11),
TN_Comment int(11))
BEGIN

UPDATE `pymesearchdb`.`tps_shop`
SET
`TN_Shop` = TN_Shop,
`TC_Name` = TC_Name,
`TC_Phone` = TC_Phone,
`TC_Mail` = TC_Mail,
`TC_Password`= TC_Password,
`TC_Likn1` = TC_Likn1,
`TC_Likn2` = TC_Likn2,
`TN_Location` = TN_Location,
`TN_CategoryShop` = TN_CategoryShop,
`TN_Comment` = TN_Comment
WHERE `TN_Shop` = TN_Shop;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_User_Delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_User_Delete`(
  TN_User INT(11))
BEGIN
DELETE FROM `pymesearchdb`.`tps_user`
WHERE `TN_User` = TN_User;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_User_Insert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_User_Insert`(
  TC_UserName VARCHAR(100),
  TC_Password VARCHAR(1000),
  TN_UserType INT(11))
BEGIN
INSERT INTO `pymesearchdb`.`tps_user`
    (`TC_UserName`,
    `TC_Password`,
    `TF_CreatedDate`,
    `TN_UserType`)
VALUES
    (TC_UserName,
     TC_Password,
     CURDATE(),
     TN_UserType);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_User_Select` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_User_Select`(
TC_UserName VARCHAR(100))
BEGIN
SELECT `tps_user`.`TN_User`,
       `tps_user`.`TC_UserName`,
       `tps_user`.`TC_Password`,
       `tps_user`.`TF_CreatedDate`
FROM `pymesearchdb`.`tps_user`
WHERE `TC_UserName` = TC_UserName;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_User_SelectLogin` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_User_SelectLogin`(
TC_UserName VARCHAR(100))
BEGIN
SELECT `tps_user`.`TC_UserName`,
       `tps_user`.`TC_Password`,
       `tps_user`.`TN_UserType`
FROM `pymesearchdb`.`tps_user`
WHERE `TC_UserName` = TC_UserName;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_PS_User_Update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PS_User_Update`(
  TN_User INT(11),
  TC_UserName VARCHAR(100),
  TC_Password VARCHAR(1000),
  TF_CreatedDate DATETIME,
  TN_UserType INT(11))
BEGIN
UPDATE `pymesearchdb`.`tps_user`
SET
	  `TC_UserName` =  TC_UserName,
      `TC_Password` =  TC_Password,
      `TF_CreatedDate` =  TF_CreatedDate,
      `TN_UserType` =  TN_UserType
WHERE `TN_User` = TN_User;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-27 14:11:05
