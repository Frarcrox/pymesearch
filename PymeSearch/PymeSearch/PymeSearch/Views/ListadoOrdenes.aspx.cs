﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using PymeSearch.Data;
using PymeSearch.Entities;

namespace PymeSearch.Views
{
    public partial class ListadoOrdenes : System.Web.UI.Page
    {
        [WebMethod(enableSession: true)]
        public static void CrearOrden(string pNombre, string pApellidos, string pTelefono, string pFechaEntrega, string pComentario, string pComercioId)
        {
            Orden orden = new Orden()
            {
                nombre = pNombre,
                apellidos = pApellidos,
                telefono = pTelefono,
                fechaEntrega = Convert.ToDateTime(pFechaEntrega),
                comentario = pComentario,
                comercio = new Comercio()
                {
                    Id = Convert.ToInt32(pComercioId)
                }
            };
            OrdenDatos.CreateOrden(orden);
        }

        [WebMethod(enableSession: true)]
        public static void ActualizarOrden(string pId, string pNombre, string pApellidos, string pTelefono, string pFechaEntrega, string pComentario, string pEstado, string pComercioId)
        {
            Orden orden = new Orden()
            {
                Id = Convert.ToInt32(pId),
                nombre = pNombre,
                apellidos = pApellidos,
                telefono = pTelefono,
                fechaEntrega = Convert.ToDateTime(pFechaEntrega),
                comentario = pComentario,
                idEstado = Convert.ToInt32(pEstado),
                comercio = new Comercio()
                {
                    Id = Convert.ToInt32(pComercioId)
                }
            };
            OrdenDatos.UpdateOrden(orden);
        }

        [WebMethod(enableSession: true)]
        public static void EliminarOrden(string pId)
        {
            OrdenDatos.DeleteOrden(Convert.ToInt32(pId));
        }

        [WebMethod(enableSession: true)]
        public static string ObtenerOrden(string pComercioId)
        {
            var result = OrdenDatos.getOrden(Convert.ToInt32(pComercioId));
            var jsonObject = JsonConvert.SerializeObject(result);
            return jsonObject;
        }
    }
}