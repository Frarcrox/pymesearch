﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResultadoBusqueda.aspx.cs" Inherits="PymeSearch.Views.ResultadoBusqueda" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>

    <script src="../Assets/templateStyle/scripts/jquery-3.5.1.min.js"></script>
    <link href="../Assets/templateStyle/css/style.css" rel="stylesheet" />
    <link href="../Assets/templateStyle/css/main-color.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/main-color.css" id="colors" />
    <script src="https://unpkg.com/leaflet@1.0.2/dist/leaflet.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.2/dist/leaflet.css" />
    <script src="../Assets/Js/Principal.js"></script>
</head>
<body>

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Slider
================================================== -->
        <div class="listing-slider mfp-gallery-container margin-bottom-0">
            <a href="../images/single-listing-01.jpg" data-background-image="../images/single-listing-01.jpg" class="item mfp-gallery" title="Title 1"></a>
            <a href="../images/single-listing-02.jpg" data-background-image="../images/single-listing-02.jpg" class="item mfp-gallery" title="Title 3"></a>
            <a href="../images/single-listing-03.jpg" data-background-image="../images/single-listing-03.jpg" class="item mfp-gallery" title="Title 2"></a>
            <a href="../images/single-listing-04.jpg" data-background-image="../images/single-listing-04.jpg" class="item mfp-gallery" title="Title 4"></a>
        </div>

        <!-- Content
================================================== -->
        <div class="container">
            <div class="row sticky-wrapper">
                <div class="col-lg-8 col-md-8 padding-right-30">

                    <!-- Titlebar -->
                    <div id="titlebar" class="listing-titlebar">
                        <div class="listing-titlebar-title">
                            <h1><strong>El Fogón</strong><span class="listing-tag">Panadería</span></h1>
                            <span>
                                <a href="#listing-location" class="listing-address">
                                    <i class="fa fa-map-marker"></i>
                                    200mts oeste de pricesmart y 500mts sur, Alajuela.
                                </a>
                            </span>
                        </div>
                        <ul class="listing-links">
                            <li><a href="#" target="_blank" class="listing-links-fb"><i class="fa fa-facebook-square"></i>Facebook</a></li>
                            <li><a href="#" target="_blank" class="listing-links-ig"><i class="fa fa-instagram"></i>Instagram</a></li>
                            <li><a href="#" target="_blank" class="listing-links-tt"><i class="fa fa-map"></i>Navegación</a></li>
                        </ul>
                    </div>

                    <!-- Overview -->
                    <div id="listing-overview" class="listing-section">
                        <div class="clearfix"></div>
                    </div>


                    <!-- Map -->
                    <div id="listing-pricing-list" class="listing-section">
                        <div id="map-container" class="fullwidth-home-map">
                            <div id="map">
                            </div>
                        </div>
                    </div>
                    <!-- Map -->


                    <!-- Reviews -->
                    <div id="listing-reviews" class="listing-section">
                        <h3 class="listing-desc-headline margin-top-75 margin-bottom-20">Comentarios <span></span></h3>

                        <div class="clearfix"></div>

                        <!-- Reviews -->
                        <section class="comments listing-reviews">
                            <ul>
                                <li>
                                    <div class="avatar">
                                        <img src="http://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&amp;s=70" alt="" />
                                    </div>
                                    <div class="comment-content">
                                        <div class="arrow-comment"></div>
                                        <div class="comment-by">
                                            Kathy Brown<span class="date">June 2019</span>
                                        </div>
                                        <p>Morbi velit eros, sagittis in facilisis non, rhoncus et erat. Nam posuere tristique sem, eu ultricies tortor imperdiet vitae. Curabitur lacinia neque non metus</p>
                                    </div>
                                </li>

                                <li>
                                    <div class="avatar">
                                        <img src="http://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&amp;s=70" alt="" />
                                    </div>
                                    <div class="comment-content">
                                        <div class="arrow-comment"></div>
                                        <div class="comment-by">
                                            John Doe<span class="date">May 2019</span>
                                        </div>
                                        <p>Commodo est luctus eget. Proin in nunc laoreet justo volutpat blandit enim. Sem felis, ullamcorper vel aliquam non, varius eget justo. Duis quis nunc tellus sollicitudin mauris.</p>
                                    </div>
                                </li>

                                <li>
                                    <div class="avatar">
                                        <img src="http://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&amp;s=70" alt="" />
                                    </div>
                                    <div class="comment-content">
                                        <div class="arrow-comment"></div>
                                        <div class="comment-by">
                                            Kathy Brown<span class="date">June 2019</span>
                                        </div>
                                        <p>Morbi velit eros, sagittis in facilisis non, rhoncus et erat. Nam posuere tristique sem, eu ultricies tortor imperdiet vitae. Curabitur lacinia neque non metus</p>
                                    </div>
                                </li>

                            </ul>
                        </section>

                        <!-- Pagination -->
                        <div class="clearfix"></div>
                        <div class="clearfix"></div>
                        <!-- Pagination / End -->
                    </div>


                    <!-- Add Review Box -->
                    <div id="add-review" class="add-review-box">

                        <!-- Add Review -->
                        <h3 class="listing-desc-headline margin-bottom-10">Agregar un comentario</h3>

                        <!-- Review Comment -->
                        <form id="add-comment" class="add-comment">
                            <fieldset>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Nombre:</label>
                                        <input type="text" value="" />
                                    </div>
                                </div>

                                <div>
                                    <label>Comentario:</label>
                                    <textarea cols="40" rows="3"></textarea>
                                </div>

                            </fieldset>

                            <button class="button">Enviar</button>
                            <div class="clearfix"></div>
                        </form>

                    </div>
                    <!-- Add Review Box / End -->
                </div>


                <!-- Sidebar
		================================================== -->
                <div class="col-lg-4 col-md-4 margin-top-75 sticky">

                    <!-- Verified Badge -->
                    <div class="verified-badge with-tip" data-tip-content="Permite ordernar productos del comercio seleccionado.">
                        <i class="sl sl-icon-check"></i>Crear orden
                    </div>

                    <!-- Coupon Widget -->
                    <div class="coupon-widget" style="background-image: url(http://localhost/listeo_html/images/single-listing-01.jpg);">
                        <a href="#" class="coupon-top">
                            <span class="coupon-link-icon"></span>
                            <h3>Por la compra de 2 baguettes, el tercero es gratis.</h3>
                            <div class="coupon-valid-untill">Válido hasta 25/12/2020</div>
                            <div class="coupon-how-to-use"><strong>Como aplicar el código?</strong> Muestra este cupón desde tu dispositivo en el comercio.</div>
                        </a>
                        <div class="coupon-bottom">
                            <div class="coupon-scissors-icon"></div>
                            <div class="coupon-code">L1ST30</div>
                        </div>
                    </div>


                    <!-- Opening Hours -->
                    <div class="boxed-widget opening-hours margin-top-35">
                        <div class="listing-badge now-open">Abierto</div>
                        <h3><i class="sl sl-icon-clock"></i>Horario</h3>
                        <ul>
                            <li>Lunes <span>9 AM - 5 PM</span></li>
                            <li>Martes <span>9 AM - 5 PM</span></li>
                            <li>Miércoles <span>9 AM - 5 PM</span></li>
                            <li>Jueves <span>9 AM - 5 PM</span></li>
                            <li>Viernes <span>9 AM - 5 PM</span></li>
                            <li>Sábado <span>9 AM - 3 PM</span></li>
                            <li>Domingo <span>Cerrado</span></li>
                        </ul>
                    </div>
                    <!-- Opening Hours / End -->
                </div>
                <!-- Sidebar / End -->
            </div>
        </div>


        <!-- Footer
================================================== -->
        <div class="copyrights">© 2019 Listeo. All Rights Reserved.</div>
        <!-- Footer / End -->

    </div>
    <!-- Wrapper / End -->

    <!-- Scripts
================================================== -->
    <script src="../Assets/templateStyle/scripts/jquery-migrate-3.3.1.min.js"></script>
    <script src="../Assets/templateStyle/scripts/mmenu.min.js"></script>
    <script src="../Assets/templateStyle/scripts/chosen.min.js"></script>
    <script src="../Assets/templateStyle/scripts/slick.min.js"></script>
    <script src="../Assets/templateStyle/scripts/rangeslider.min.js"></script>
    <script src="../Assets/templateStyle/scripts/magnific-popup.min.js"></script>
    <script src="../Assets/templateStyle/scripts/waypoints.min.js"></script>
    <script src="../Assets/templateStyle/scripts/counterup.min.js"></script>
    <script src="../Assets/templateStyle/scripts/jquery-ui.min.js"></script>
    <script src="../Assets/templateStyle/scripts/tooltips.min.js"></script>
    <script src="../Assets/templateStyle/scripts/custom.js"></script>


    <!-- Booking Widget - Quantity Buttons -->
    <script src="scripts/quantityButtons.js"></script>

    <!-- Date Range Picker - docs: http://www.daterangepicker.com/ -->
    <script src="scripts/moment.min.js"></script>
    <script src="scripts/daterangepicker.js"></script>
    <script>
        // Calendar Init
        $(function () {
            $('#date-picker').daterangepicker({
                "opens": "left",
                singleDatePicker: true,

                // Disabling Date Ranges
                isInvalidDate: function (date) {
                    // Disabling Date Range
                    var disabled_start = moment('09/02/2018', 'MM/DD/YYYY');
                    var disabled_end = moment('09/06/2018', 'MM/DD/YYYY');
                    return date.isAfter(disabled_start) && date.isBefore(disabled_end);

                    // Disabling Single Day
                    // if (date.format('MM/DD/YYYY') == '08/08/2018') {
                    //     return true; 
                    // }
                }
            });
        });

        // Calendar animation
        $('#date-picker').on('showCalendar.daterangepicker', function (ev, picker) {
            $('.daterangepicker').addClass('calendar-animated');
        });
        $('#date-picker').on('show.daterangepicker', function (ev, picker) {
            $('.daterangepicker').addClass('calendar-visible');
            $('.daterangepicker').removeClass('calendar-hidden');
        });
        $('#date-picker').on('hide.daterangepicker', function (ev, picker) {
            $('.daterangepicker').removeClass('calendar-visible');
            $('.daterangepicker').addClass('calendar-hidden');
        });
    </script>


    <!-- Replacing dropdown placeholder with selected time slot -->
    <script>
        $(".time-slot").each(function () {
            var timeSlot = $(this);
            $(this).find('input').on('change', function () {
                var timeSlotVal = timeSlot.find('strong').text();

                $('.panel-dropdown.time-slots-dropdown a').html(timeSlotVal);
                $('.panel-dropdown').removeClass('active');
            });
        });
    </script>


    <!-- Style Switcher
================================================== -->
    <script src="scripts/switcher.js"></script>

    <div id="style-switcher">
        <h2>Color Switcher <a href="#"><i class="sl sl-icon-settings"></i></a></h2>

        <div>
            <ul class="colors" id="color1">
                <li><a href="#" class="main" title="Main"></a></li>
                <li><a href="#" class="blue" title="Blue"></a></li>
                <li><a href="#" class="green" title="Green"></a></li>
                <li><a href="#" class="orange" title="Orange"></a></li>
                <li><a href="#" class="navy" title="Navy"></a></li>
                <li><a href="#" class="yellow" title="Yellow"></a></li>
                <li><a href="#" class="peach" title="Peach"></a></li>
                <li><a href="#" class="beige" title="Beige"></a></li>
                <li><a href="#" class="purple" title="Purple"></a></li>
                <li><a href="#" class="celadon" title="Celadon"></a></li>
                <li><a href="#" class="red" title="Red"></a></li>
                <li><a href="#" class="brown" title="Brown"></a></li>
                <li><a href="#" class="cherry" title="Cherry"></a></li>
                <li><a href="#" class="cyan" title="Cyan"></a></li>
                <li><a href="#" class="gray" title="Gray"></a></li>
                <li><a href="#" class="olive" title="Olive"></a></li>
            </ul>
        </div>

    </div>
    <!-- Style Switcher / End -->


</body>
</html>
